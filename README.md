# Photoresistor

Danny Yang

### What is this repository for? ###

The purpose of the Photoresistor is to sense the change in the light outputed by our environment. If it gets to dark our camera on our machine will not see anything.
We need the photoresistor to indicate when it should turn on it's led lights. 

### How do I get set up? ###

To set up we set the photoresistor in series with 470 ohm resistor. Then you connect the 5volt pin of your microcontroller to the positive end of the photoresistor
you then connect an analog pin of your choice in between the resistor and photoresistor. You then connect the end of the resistor to ground. To see the the changing lights it sense. 
You use the analog.Read function and direct it to the analog pin you set it to. 


### Contribution guidelines ###



(n.d.). Retrieved December 06, 2017, from https://www.arduino.cc/en/Tutorial/AnalogInput
(LCD16X2 v2)


### Who do I talk to? ###

Danny Yang